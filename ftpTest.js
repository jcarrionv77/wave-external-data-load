var sf = require('jsforce');
var fs = require('fs');
var Client2 = require('ssh2-sftp-client');

var Client = require('ssh2').Client;
var connSettings = {
     host: process.env.host,
     username: process.env.username,
     password: process.env.password
     // You can use a key file too, read the ssh2 documentation
};




var connSF = new sf.Connection({
  // you can change loginUrl to connect to sandbox or prerelease env. 
  loginUrl : process.env.LOGIN_URL 
});

var remotePathToList = '/';

var conn = new Client();

conn.on('ready', function() {

    conn.sftp(function(err, sftp) {

        if (err){
          throw err;
        } 
        var moveFrom = "CaseData.csv";
        var moveTo = "/tmp/CaseData.csv";


        sftp.readdir(remotePathToList, function(err, list) {
                if (err)
                {
                   throw err;
                }
                //List the directory in the console
                //console.dir(list);
                // Do not forget to close the connection, otherwise you'll get troubles


                 sftp.fastGet(moveFrom, moveTo , {}, function(downloadError){
                      if(downloadError)
                      {
                        console.log("downloadError " + downloadError);
                        conn.end();
                        throw downloadError;
                      } 

                      //console.log("Succesfully download");

                      require.extensions['.json'] = function (module, filename) {
                          module.exports = fs.readFileSync(filename, 'utf8');
                      };

                      require.extensions['.csv'] = function (module, filename) {
                          module.exports = fs.readFileSync(filename, 'utf8');
                      };

                      //var formato = 'ew0KICAiZmlsZUZvcm1hdCI6IHsNCiAgICAiY2hhcnNldE5hbWUiOiAiVVRGLTgiLA0KICAgICJmaWVsZHNEZWxpbWl0ZWRCeSI6ICIsIiwNCiAgICAiZmllbGRzRW5jbG9zZWRCeSI6ICJcIiIsDQogICAgIm51bWJlck9mTGluZXNUb0lnbm9yZSI6IDENCiAgfSwNCiAgIm9iamVjdHMiOiBbDQogICAgew0KICAgICAgImNvbm5lY3RvciI6ICJBY21lQ1NWQ29ubmVjdG9yIiwNCiAgICAgICJkZXNjcmlwdGlvbiI6ICIiLA0KICAgICAgImZpZWxkcyI6IFsNCiAgICAgICAgew0KICAgICAgICAgICJkZXNjcmlwdGlvbiI6ICIiLA0KICAgICAgICAgICJmdWxseVF1YWxpZmllZE5hbWUiOiAiU2FsZXNEYXRhLk5hbWUiLA0KICAgICAgICAgICJpc011bHRpVmFsdWUiOiBmYWxzZSwNCiAgICAgICAgICAiaXNTeXN0ZW1GaWVsZCI6IGZhbHNlLA0KICAgICAgICAgICJpc1VuaXF1ZUlkIjogdHJ1ZSwNCiAgICAgICAgICAibGFiZWwiOiAiQWNjb3VudCBOYW1lIiwNCiAgICAgICAgICAibmFtZSI6ICJOYW1lIiwNCiAgICAgICAgICAidHlwZSI6ICJUZXh0Ig0KICAgICAgICB9LA0KICAgICAgICB7DQogICAgICAgICAgImRlZmF1bHRWYWx1ZSI6ICIwIiwNCiAgICAgICAgICAiZGVzY3JpcHRpb24iOiAiIiwNCiAgICAgICAgICAiZm9ybWF0IjogIiQjLCMjMC4wMCIsDQogICAgICAgICAgImZ1bGx5UXVhbGlmaWVkTmFtZSI6ICJTYWxlc0RhdGEuQW1vdW50IiwNCiAgICAgICAgICAiaXNTeXN0ZW1GaWVsZCI6IGZhbHNlLA0KICAgICAgICAgICJpc1VuaXF1ZUlkIjogZmFsc2UsDQogICAgICAgICAgImxhYmVsIjogIk9wcG9ydHVuaXR5IEFtb3VudCIsDQogICAgICAgICAgIm5hbWUiOiAiQW1vdW50IiwNCiAgICAgICAgICAicHJlY2lzaW9uIjogMTAsDQogICAgICAgICAgInNjYWxlIjogMiwNCiAgICAgICAgICAidHlwZSI6ICJOdW1lcmljIg0KICAgICAgICB9LA0KICAgICAgICB7DQogICAgICAgICAgImRlc2NyaXB0aW9uIjogIiIsDQogICAgICAgICAgImZpc2NhbE1vbnRoT2Zmc2V0IjogMCwNCiAgICAgICAgICAiZm9ybWF0IjogIk1NL2RkL3l5eXkiLA0KICAgICAgICAgICJmdWxseVF1YWxpZmllZE5hbWUiOiAiU2FsZXNEYXRhLkNsb3NlRGF0ZSIsDQogICAgICAgICAgImlzU3lzdGVtRmllbGQiOiBmYWxzZSwNCiAgICAgICAgICAiaXNVbmlxdWVJZCI6IGZhbHNlLA0KICAgICAgICAgICJsYWJlbCI6ICJPcHBvcnR1bml0eSBDbG9zZSBEYXRlIiwNCiAgICAgICAgICAibmFtZSI6ICJDbG9zZURhdGUiLA0KICAgICAgICAgICJ0eXBlIjogIkRhdGUiDQogICAgICAgIH0NCiAgICAgIF0sDQogICAgICAiZnVsbHlRdWFsaWZpZWROYW1lIjogIlNhbGVzRGF0YSIsDQogICAgICAibGFiZWwiOiAiU2FsZXMgRGF0YSIsDQogICAgICAibmFtZSI6ICJTYWxlc0RhdGEiDQogICAgfQ0KICBdDQp9';

                      var formatoJSON = require("./formatoCase.json");
                      var buffer = new Buffer(formatoJSON);
                      var formato = buffer.toString('base64');

                      var csv = require("/tmp/CaseData.csv");
                      var bufferCSV = new Buffer(csv);
                      var csvFile = bufferCSV.toString('base64');

                      //console.log("bufferCSV es: " + bufferCSV);
                      //console.log("csvFile es: " + csvFile);


                      //console.log(csvFile);


                    connSF.login(process.env.USER, process.env.PASS, function(err, userInfo) {
                      if (err) { return console.error(err); }
                      // Now you can get the access token and instance URL information. 
                      // Save them to establish connection next time. 
                      
                      //console.log(connSF.accessToken);
                      //console.log(connSF.instanceUrl);
                      // logged in user property 
                      //console.log("User ID: " + userInfo.id);
                      //console.log("Org ID: " + userInfo.organizationId);


                      connSF.sobject("InsightsExternalData").create({  Format : 'csv', EdgemartAlias : 'CaseExtData', 
                                                                     Operation : 'Upsert', Action : 'None', 
                                                                     MetadataJson : formato }, function(err, ret) {
                        if (err || !ret.success) { 
                           console.error(err, ret); 
                        }
                        else {
                          //console.log("Created record id : " + ret.id);
                          //console.log("csvFile es: " + csvFile);

                          connSF.sobject("InsightsExternalDataPart").create({  InsightsExternalDataId : ret.id, PartNumber : '1', 
                                                                         DataFile : csvFile}, function(err, retDataPart) {
                            if (err || !retDataPart.success) { 
                               console.error(err, retDataPart); 
                            }
                            else {
                              //console.log("Created record id : " + retDataPart.id);
                              //console.log("Update record id : " + ret.id);

                              connSF.sobject("InsightsExternalData").update({ 
                                Id : ret.id,
                                Action : 'Process'
                              }, function(err, retUpd) {
                                if (err || !retUpd.success) { return console.error(err, ret); }
                                console.log('Updated Successfully : ' + retUpd.id);
                                exit();
                                // ... 
                              });

                            }
                            
                            // ... 
                          });

                        }
                        
                        
                        // ... 
                      });
                    });


                      //conn.end();
                  });



                
         });


    });
}).connect(connSettings);




function exit()
{
    conn.end();
    connSF.logout(function(err) {
    if (err) { return console.error(err); }
    // now the session has been expired. 
    console.log("logout");
  });
}

